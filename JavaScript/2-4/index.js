document.querySelector("#solve").addEventListener("click",function(){
    var maxNum = document.querySelector('#num').value;
    var answerContainer = document.querySelector('#answers');
    var answers="";
    
    if (maxNum < 2) {
        answerContainer.innerHTML = 'Enter atleast number 2 and up';
    } else {
        for(var i = 2; i <= maxNum; i++) {
            gettingPrimeNumbers(i);
        }        
    }
    
    function gettingPrimeNumbers(num) {
        for(var k = 2; k < num; k++) {
            if (num % k == 0) {
                return;
            }
        }
        answerContainer.innerHTML = answers += " | " + k + " | ";
    }
});