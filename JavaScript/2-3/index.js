document.querySelector("#solve").addEventListener("click",function(){
    var n1 = document.querySelector('#n1').value;
    var n2 = document.querySelector('#n2').value;

    var oper = document.querySelector('#operators').value;
    switch (oper) {
        case "+":
            document.querySelector('#result').value = +n1 + +n2;
            break;
        case "-":
            document.querySelector('#result').value = n1 - n2;
            break;
        case "X":
            document.querySelector('#result').value = n1 * n2;
            break;
        case "/":
            document.querySelector('#result').value = n1 / n2;
            break;                
    
        default:
            break;
    }
});

