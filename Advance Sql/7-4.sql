SELECT 
e.id, 
e.first_name, 
e.middle_name, 
e.last_name, 
e.birth_date, 
e.hire_date, 
CASE 
	WHEN p.id = 1 THEN 'Chief Executive Officer'
	WHEN p.id = 2 THEN 'Chief Technical Officer'
	WHEN p.id = 3 THEN 'Chief Financial Officer'
	ELSE p.name
END as position,
d.name as 'Department'
FROM employees e
JOIN departments d 
	ON d.id = e.department_id
JOIN employee_positions ep
	ON ep.employee_id = e.id
JOIN positions p
	ON p.id = ep.position_id