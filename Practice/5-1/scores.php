<?php require_once 'views-include/header.php'; require_once 'includes/index.inc.php'; ?>

<div class="card col-lg-8 mx-auto mt-4">
    <h3 class="card-header text-center">My Scores</h3>
    <div class="card-body">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">Topic</th>
                    <th scope="col">My Score</th>
                    <th scope="col">Date Answered</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($dataResultsAll as $quiz): ?>
                <tr>
                    <th scope="row"><?= $quiz['description'] ?></th>
                    <td><?= $quiz['score'] ?></td>
                    <td><?= $quiz['date_created'] ?></td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>

<?php require_once 'views-include/footer.php' ?>
