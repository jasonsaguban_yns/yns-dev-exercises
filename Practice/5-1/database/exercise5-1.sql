-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2020 at 09:56 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exercise5-1`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `question` text DEFAULT NULL,
  `date_created` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `topic_id`, `question`, `date_created`) VALUES
(1, 1, 'Entering Comments is a useless task, it will  not help in anyway.', '2020-09-15 09:25:44'),
(2, 1, 'What is the meaning of php?', '2020-09-15 09:25:44'),
(3, 1, 'A Syntax Error is ? ', '2020-09-15 09:25:44'),
(4, 1, 'One loop inside the body of another loop is called', '2020-09-15 09:25:44'),
(5, 1, 'A do while and a while loop are the same', '2020-09-15 09:25:44'),
(6, 1, 'Int hold decimal numbers', '2020-09-15 09:25:44'),
(7, 1, 'A short sections of code written to complete a task. ', '2020-09-15 09:25:44'),
(8, 1, 'What command do you use to output data to the screen?', '2020-09-15 09:25:44'),
(9, 1, 'What is FIFO?', '2020-09-15 09:25:44'),
(10, 1, 'A memory location that holds a single letter or number.  ', '2020-09-15 09:25:44');

-- --------------------------------------------------------

--
-- Table structure for table `questions_answers`
--

CREATE TABLE `questions_answers` (
  `id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `choice_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `questions_answers`
--

INSERT INTO `questions_answers` (`id`, `question_id`, `choice_id`, `date_created`) VALUES
(1, 1, 2, '2020-09-15 09:38:19'),
(2, 2, 6, '2020-09-15 09:38:19'),
(3, 3, 8, '2020-09-15 09:38:19'),
(4, 4, 11, '2020-09-15 09:38:19'),
(5, 5, 14, '2020-09-15 09:38:19'),
(6, 6, 17, '2020-09-15 09:38:19'),
(7, 7, 21, '2020-09-15 09:38:19'),
(8, 8, 23, '2020-09-15 09:38:19'),
(9, 9, 27, '2020-09-15 09:38:19'),
(10, 10, 30, '2020-09-15 09:38:19');

-- --------------------------------------------------------

--
-- Table structure for table `question_choices`
--

CREATE TABLE `question_choices` (
  `id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `choice_description` text DEFAULT NULL,
  `date_created` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `question_choices`
--

INSERT INTO `question_choices` (`id`, `question_id`, `choice_description`, `date_created`) VALUES
(1, 1, 'True', '2020-09-15 09:33:42'),
(2, 1, 'False', '2020-09-15 09:33:42'),
(3, 1, 'I don\'t know', '2020-09-15 09:33:42'),
(4, 2, 'Philippines', '2020-09-15 09:33:42'),
(5, 2, 'Peso', '2020-09-15 09:33:42'),
(6, 2, 'Hypertext Processor', '2020-09-15 09:33:42'),
(7, 3, 'An error you will never find', '2020-09-15 09:33:42'),
(8, 3, 'An error caused by language rules being broken.', '2020-09-15 09:33:42'),
(9, 3, 'An error due to user error', '2020-09-15 09:33:42'),
(10, 4, 'Loop loop', '2020-09-15 09:33:42'),
(11, 4, 'Nested', '2020-09-15 09:33:42'),
(12, 4, 'Double loops', '2020-09-15 09:33:42'),
(13, 5, 'True', '2020-09-15 09:33:42'),
(14, 5, 'False', '2020-09-15 09:33:42'),
(15, 5, 'I don\'t know', '2020-09-15 09:33:42'),
(16, 6, 'True', '2020-09-15 09:33:42'),
(17, 6, 'False', '2020-09-15 09:33:42'),
(18, 6, 'I don\'t know', '2020-09-15 09:33:42'),
(19, 7, 'Buffer', '2020-09-15 09:33:42'),
(20, 7, 'Array', '2020-09-15 09:33:42'),
(21, 7, 'Function', '2020-09-15 09:33:42'),
(22, 8, 'Cin', '2020-09-15 09:33:42'),
(23, 8, 'Cout>>', '2020-09-15 09:33:42'),
(24, 8, 'Output>>', '2020-09-15 09:33:42'),
(25, 9, 'First in Few Out', '2020-09-15 09:33:42'),
(26, 9, 'Few In Few out', '2020-09-15 09:33:42'),
(27, 9, 'First In First Out', '2020-09-15 09:33:42'),
(28, 10, 'Double', '2020-09-15 09:33:42'),
(29, 10, 'Int', '2020-09-15 09:33:42'),
(30, 10, 'Char', '2020-09-15 09:33:42');

-- --------------------------------------------------------

--
-- Table structure for table `scores`
--

CREATE TABLE `scores` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `date_created` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`id`, `description`, `date_created`) VALUES
(1, 'Entrance Examination', '2020-09-14 16:46:26');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `username`, `password`, `date_created`) VALUES
(3, 'Jason', 'Saguban', 'saguban17@gmail.com', 'user1', '$2y$10$Auc10TQewU9odL4Bb5.rru/9V8jKDFx2cj.hnFzV5sGRkbjGYGHz2', '2020-09-14 13:17:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions_answers`
--
ALTER TABLE `questions_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_choices`
--
ALTER TABLE `question_choices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scores`
--
ALTER TABLE `scores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `questions_answers`
--
ALTER TABLE `questions_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `question_choices`
--
ALTER TABLE `question_choices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `scores`
--
ALTER TABLE `scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
