<?php require_once 'views-include/header.php'; require_once 'includes/index.inc.php'; ?>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
      <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" role="img" focusable="false"><rect width="100%" height="100%" fill="#777"/></svg>
        <div class="container">
          <div class="carousel-caption text-left">
            <h1>Example headline.</h1>
            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>            
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" role="img" focusable="false"><rect width="100%" height="100%" fill="#777"/></svg>

        <div class="container">
          <div class="carousel-caption">
            <h1>Another example headline.</h1>
            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>            
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" role="img" focusable="false"><rect width="100%" height="100%" fill="#777"/></svg>

        <div class="container">
          <div class="carousel-caption text-right">
            <h1>One more for good measure.</h1>
            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>            
          </div>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="container marketing">

    <div class="row">

      <div class="card col-lg-5 mx-auto">
        <h2>Quizzes available for you</h2>
        <div class="h5 card-header primary">
            Topics:
        </div>
        <ul class="card-body list-group">
            <?php foreach($dataTopics as $key => $quiz): ?>
                <a href=<?= 'quizpage.php?id='.$quiz['id'] ?> class="list-group-item"><?= ($key + 1 ).". ".$quiz['description'] ?></a>
            <?php endforeach ?>    
            </ul>
        <p><a class="btn btn-secondary mt-2 float-right" href="quizzes.php" role="button">View details &raquo;</a></p>
      </div>

      <div class="card col-lg-5 mx-auto">
        <h2>Answered Quizzes</h2>
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">Topic</th>
                    <th scope="col">My Score</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($dataResults as $key => $quiz): ?>
                <tr>
                    <th scope="row"><?= $quiz['description'] ?></th>
                    <td><?= $quiz['score'] ?></td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        <p><a class="btn btn-secondary mt-2 float-right" href="scores.php" role="button">View details &raquo;</a></p>
      </div>
    </div>

<?php require_once 'views-include/footer.php' ?>