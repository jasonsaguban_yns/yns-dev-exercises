<?php require_once 'views-include/header.php'; ?>

<div class="card col-lg-8 mx-auto mt-4">
    <h3 class="card-header text-center">About Q U I Z Z E R</h3>
    <div class="card-body">
        <p class="card-text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Explicabo recusandae officia, impedit soluta eligendi ipsum nostrum optio, in fuga nulla pariatur quod iste repellat adipisci hic quasi deserunt sequi? Porro!
        Aperiam nostrum reiciendis enim doloremque aspernatur suscipit possimus quasi? Quos quaerat voluptate cupiditate, ratione harum quod praesentium quis. Modi quibusdam dolore voluptas sed reiciendis optio sapiente molestiae obcaecati, sit nobis.
        Perferendis deleniti vel saepe dolorum quae, nesciunt ea odio dolor sed alias! Temporibus quaerat adipisci autem quae sapiente quisquam, animi porro earum aspernatur nesciunt nihil vero quia quis optio quos.
        Totam aut ea, aperiam dolor repellat tempora adipisci voluptatum consequuntur! Odit nisi distinctio autem fugiat fuga quis, iusto voluptatibus vel voluptatum ipsum ad dignissimos, tempore pariatur odio, excepturi laborum quisquam.
        Commodi temporibus sunt vitae, nesciunt eaque eius, fugit veritatis culpa libero inventore possimus pariatur autem quae hic sint doloremque dolor? Tempora provident assumenda consequatur eligendi nostrum est animi nisi sint.</p>
</div>

<?php require_once 'views-include/footer.php' ?>