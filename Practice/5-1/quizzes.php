<?php require_once 'views-include/header.php'; require_once 'includes/index.inc.php'; ?>

<div class="card col-lg-8 mx-auto mt-4">
    <h3 class="card-header text-center">My Quizzes</h3>
    <div class="card-body">
        <h5 class="card-title">Topics:</h5>
        <ul class="list-group list-group-flush">
            <?php foreach($dataTopicsAll as $key => $quiz): ?>
                <a href=<?= 'quizpage.php?id='.$quiz['id'] ?> class="list-group-item"><?= ($key + 1 ).". ".$quiz['description'] ?></a>
            <?php endforeach ?>    
        </ul>
    </div>
</div>

<?php require_once 'views-include/footer.php' ?>