<?php 
session_start(); 
if(empty($_SESSION['firstName'])){
    header('Location: ./login.php');
}
?>
<header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="./index.php">Q U I Z Z E R</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto mb-2 mb-md-0">
          <li class="nav-item active">
            <a class="nav-link" aria-current="page" href="./index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./quizzes.php">My Quizzes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./scores.php">My Scores</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./about.php">About</a>
          </li>
        </ul>
        <form class="d-flex">
            <span class="navbar-brand mb-0 h1"> Hello <?= ucfirst($_SESSION['firstName']) .'!' ?> </span>
            <a href="./includes/logout.inc.php" class="btn btn-outline-danger">Logout</a>
        </form>
      </div>
    </div>
  </nav>
</header>