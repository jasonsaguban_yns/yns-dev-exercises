<?php 
require_once 'views-include/header-login.php';
session_start(); 
if(!empty($_SESSION['data'])){
    $data = $_SESSION['data'];
}
?>


<form class="form-signin" method="post" action="includes/register.inc.php">
      <h1 class="h1 mb-4 font-weight-normal">Q U I Z Z E R</h1>
      
      <input type="text" name="username"  class="form-control <?= (!empty($data['username_err'])) ? 'is-invalid' : '' ?>" value="<?= !(empty($data['username'])) ? $data['username'] :'' ?>" placeholder="Username" required autofocus>
      <span class="invalid-feedback "><?= (!empty($data['username_err'])) ?  $data['username_err'] :'' ?></span>
      
      <input type="password" name="password" class="form-control <?= (!empty($data['password_err'])) ? 'is-invalid' : '' ?>" value="<?= (!empty($data['password'])) ? $data['password'] :'' ?>" placeholder="Password" required>
      <span class="invalid-feedback "><?= (!empty($data['password_err'])) ? $data['password_err'] :'' ?></span>

      <input type="password" name="password_confirm" class="form-control <?= (!empty($data['confirm_pass_err'])) ? 'is-invalid' : '' ?>" value="<?= (!empty($data['confirm_pass'])) ? $data['confirm_pass'] : '' ?>" placeholder="Confirm Password" required>
      <span class="invalid-feedback mb-2"><?= (!empty($data['confirm_pass_err'])) ? $data['confirm_pass_err'] :'' ?></span>
            
      <input type="email" name="email" class="form-control <?= (!empty($data['email_err'])) ? 'is-invalid' : '' ?>" value="<?= (!empty($data['email'])) ? $data['email'] :'' ?>" placeholder="Email" required>
      <span class="invalid-feedback"><?= (!empty($data['email_err'])) ? $data['email_err'] :'' ?></span>
      
      <input type="text" name="firstName" class="form-control <?= (!empty($data['fname_err'])) ? 'is-invalid' : '' ?>" value="<?= (!empty($data['fname'])) ? $data['fname'] :'' ?>" placeholder="First Name" required>
      <span class="invalid-feedback"><?= (!empty($data['fname_err'])) ? $data['fname_err'] :'' ?></span>
      
      <input type="text" name="lastName" class="form-control <?= (!empty($data['lname_err'])) ? 'is-invalid' : '' ?>" value="<?= (!empty($data['lname'])) ?  $data['lname'] :'' ?>" placeholder="Last Name" required>
      <span class="invalid-feedback"><?= (!empty($data['lname_err'])) ?  $data['lname_err'] :'' ?></span>

      <button class="btn my-3 btn-lg btn-primary btn-block" type="submit">Register</button>
      <a href="login.php">Already have an account? Login now!</a>
</form>
<?php unset($_SESSION['data']); ?>

<?php require_once 'views-include/footer.php' ?>