<?php require_once 'views-include/header.php'; require_once 'includes/quizpage.inc.php'; ?>

<form action=<?= "includes/quizchecker.inc.php?topicId=".$topicId ?>  method='post' onsubmit="return confirm('Do you really want to submit your quiz?');">

    <div class="card col-lg-8 mx-auto mt-4" >
        <h3 class="card-header text-center"><?= $topicDescription ?></h3> 
        <?php foreach ($topicQuestions as $key => $question): ?>

            <p class="card-text mt-3 ml-3"><?= ($key + 1).". ".$question['question'] ?></p>
                <?php $choices=getChoices($question['question_id'],$topicId); foreach($choices as $choice):?>
                    <div class="form-check">
                        <input type="radio" class="form-check-input ml-3" name=<?= $choice['question_id'] ?> value=<?= $choice['choice_id'] ?>>
                        <label class="form-check-label ml-2 mb-3"><?= $choice['choice_description'] ?></label><br>
                    </div>
                <?php endforeach; ?>
                <hr class="featurette-divider my-3">
        <?php endforeach ?>
        <button class="btn btn-success">Submit Answers</button>
    </div>

</form>

<?php require_once 'views-include/footer.php' ?>