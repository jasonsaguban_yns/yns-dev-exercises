<?php 
$host="localhost";
$dbuser = "root";
$dbpassword = "";
$dbname="exercise5-1";

$dsn='mysql:host='.$host.';dbname='.$dbname;

$pdo=new PDO($dsn, $dbuser, $dbpassword);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
