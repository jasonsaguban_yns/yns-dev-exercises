<?php
require_once 'db.inc.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $data=[
        'username' => trim($_POST['username']),
        'password' => trim($_POST['password']),

        'username_err' => '',
        'password_err' => ''
    ];

    //validations

    if(empty($data['username'])){
        $data['username_err']='Please enter a username.';
    } else{
        //Checking if the username is already taken

        $sql='SELECT * FROM users WHERE username= :username OR email=:username';
        $stmt= $pdo->prepare($sql);
        $stmt->execute(['username' => $data['username']]);
        $userCount = $stmt->rowCount();
        $userData= $stmt->fetch();
        if ($userCount < 1){
            $data['username_err']='User does not exist.';  
        }
    }

    if(empty($data['password'])){
        $data['password_err']='Please enter a password.';
    }

    //Make sure errors are empty
    if(empty($data['password_err']) && empty($data['username_err'])){
        //if there is no error
        if(password_verify($data['password'],$userData['password'])){
            //if password is correct
            session_start();
            $_SESSION['uid'] = $userData['id'];
            $_SESSION['firstName'] = $userData['first_name'];
            $_SESSION['lastName'] = $userData['last_name'];
            $_SESSION['email'] = $userData['email'];
            header('Location: ../index.php');

            if(!empty($_SESSION['data'])){
                unset($_SESSION['data']);
            }
        } else{
            //if password is NOT correct
            $data['password_err']='Wrong password';
            session_start();
            $_SESSION['data']=$data;
            header('Location: ../login.php');
        }

    } else{
        //if there is/are error/s
        session_start();
        $_SESSION['data']=$data;
        header('Location: ../login.php');
    }

}else{
    header('Location: login.php');
}