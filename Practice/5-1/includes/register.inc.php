<?php 

require_once 'db.inc.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $data=[
        'fname' => trim($_POST['firstName']),
        'lname' => trim($_POST['lastName']),
        'username' => trim($_POST['username']),
        'password' => trim($_POST['password']),
        'confirm_pass' => trim($_POST['password_confirm']),
        'email' => trim($_POST['email']),

        'fname_err' =>'',
        'lname_err' =>'',
        'username_err' =>'',
        'password_err' =>'',
        'confirm_pass_err' =>'',
        'email_err' =>'',
    ];

    //validation

    if(empty($data['fname'])){
        $data['fname_err']='Please enter your first name.';
    }

    if(empty($data['lname'])){
        $data['lname_err']='Please enter your last name.';
    }

    if(empty($data['username'])){
        $data['username_err']='Please enter a username.';
    } else{
        //Checking if the username is already taken
        $sql='SELECT * FROM users WHERE username= :username';
        $stmt= $pdo->prepare($sql);
        $stmt->execute(['username' => $data['username']]);
        $userCount = $stmt->rowCount();
        if ($userCount > 0)
            $data['username_err']='The username is already taken.';        
    }

    if(empty($data['password'])){
        $data['password_err']='Please enter a password.';
    } elseif(strlen($data['password']) < 8){
        $data['password_err']='Password must be at least 8 characters'; 
    } elseif(empty($data['confirm_pass'])){
        $data['confirm_pass_err']='Please re-enter your password.';
    } elseif($data['password'] != $data['confirm_pass']){
        $data['confirm_pass_err']='Password do not match.';
        $data['password_err']='Password do not match.';
    }


    if(empty($data['email'])){
        $data['email_err']='Please enter your email.';
      }
    
    //Make sure errors are empty
    if(empty($data['email_err']) && empty($data['fname_err']) && empty($data['lname_err']) && empty($data['password_err']) && empty($data['confirm_pass_err']) && empty($data['username_err'])){
        //If no errors

        //hash the password
        $data['password']= password_hash($data['password'],PASSWORD_DEFAULT);

        // register the user
        $sql='INSERT INTO users(first_name,last_name,username,password,email) 
        VALUES(:first_name,:last_name,:username,:password,:email)';
        $stmt= $pdo->prepare($sql);
        $stmt->execute([
            'first_name' => $data['fname'],
            'last_name' => $data['lname'],
            'username' => $data['username'],
            'password' => $data['password'],
            'email' => $data['email'],
            ]);
        header('Location: ../login.php');
        
        if(!empty($_SESSION['data'])){
            unset($_SESSION['data']);
        }
    } else{
        //if there is/are error/s
        session_start();
        $_SESSION['data']=$data;
        header('Location: ../register.php');
    }

} else{
    header('Location: ../register.php');
}