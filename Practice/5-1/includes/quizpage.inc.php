<?php
require_once 'db.inc.php';

$topicId = $_GET['id'];

$sql="SELECT 
        t.description as 'topic',
        q.id as 'question_id',
        q.question
        FROM questions q
        JOIN topics t
        ON t.id = q.topic_id
        WHERE topic_id=".$topicId;
$stmt= $pdo->prepare($sql);
$stmt->execute();
$topicQuestions = $stmt->fetchAll();
if(!isset($topicQuestions) || empty($topicQuestions)){
    header('Location: ./quizzes.php');
}
$topicDescription= $topicQuestions[0]['topic'];
shuffle($topicQuestions);


function getChoices($questionId,$topicId){
    include 'db.inc.php';
    $sql="SELECT
    t.id as 'topic_id',
    q.id as 'question_id',
    q.question,
    qc.id as 'choice_id',
    qc.choice_description
    FROM questions q
    JOIN question_choices qc
        ON qc.question_id = q.id
    JOIN questions_answers qa
        ON qa.question_id = q.id
    JOIN topics t
        ON t.id = q.topic_id
    WHERE q.id = ".$questionId ." and t.id =".$topicId;
    $stmt= $pdo->prepare($sql);
    $stmt->execute();
    $questionChoices = $stmt->fetchAll();
    shuffle($questionChoices);
    return $questionChoices;
}

