<?php
session_start();
unset($_SESSION['uid']);
unset($_SESSION['firstName']);
unset($_SESSION['lastName']);
unset($_SESSION['email']);
session_destroy();
header('Location: ../login.php');