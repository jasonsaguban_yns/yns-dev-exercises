<?php 
require_once 'db.inc.php';
$topicId = $_GET['topicId'];
//checking the answers
$score =0 ;
foreach ($_POST as $name => $val)
{
    $sql="SELECT 
            *
            FROM questions_answers
            WHERE question_id=".$name." and choice_id= ".$val;

    $stmt= $pdo->prepare($sql);
    $stmt->execute();
    $checker = $stmt->rowCount();
    if($checker > 0){
        $score++;
    }
}
//saving the score to the database
session_start();
$sql='INSERT INTO scores(user_id,topic_id,score) 
        VALUES(:user_id,:topic_id,:score)';
$stmt= $pdo->prepare($sql);
$stmt->execute([
    'user_id' => $_SESSION['uid'],
    'topic_id' => $topicId,
    'score' => $score
    ]);

header('Location: ../scores.php');
