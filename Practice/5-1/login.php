<?php require_once 'views-include/header-login.php'; 
session_start(); 
if(!empty($_SESSION['data'])){
    $data = $_SESSION['data'];
}
?>

<form class="form-signin" method="post" action="includes/login.inc.php">
      <h1 class="h1 mb-4 font-weight-normal">Q U I Z Z E R</h1>
      
      <input type="text" name="username" class="form-control <?= (!empty($data['username_err'])) ? 'is-invalid' : '' ?>" value="<?= !(empty($data['username'])) ? $data['username'] :'' ?>" placeholder="Username or Email" required autofocus>
      <span class="invalid-feedback "><?= (!empty($data['username_err'])) ?  $data['username_err'] :'' ?></span>
      
      <input type="password" name="password" class="form-control <?= (!empty($data['password_err'])) ? 'is-invalid' : '' ?>" value="<?= (!empty($data['password'])) ? $data['password'] :'' ?>" placeholder="Password" required>
      <span class="invalid-feedback "><?= (!empty($data['password_err'])) ? $data['password_err'] :'' ?></span>

      <button class="btn my-3 btn-lg btn-primary btn-block" type="submit">Login</button>

      <a href="register.php">No account yet? Register now!</a>
</form>

<?php unset($_SESSION['data']); ?>
<?php require_once 'views-include/footer.php' ?>