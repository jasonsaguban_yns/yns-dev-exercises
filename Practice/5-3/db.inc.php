<?php 
function getData($sql){
    $host="localhost";
    $dbuser = "root";
    $dbpassword = "";
    $dbname="exercise5-3";

    $dsn='mysql:host='.$host.';dbname='.$dbname;

    $pdo=new PDO($dsn, $dbuser, $dbpassword);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    $stmt= $pdo->prepare($sql);
    $stmt->execute();
    $data = $stmt->fetchAll();
    return $data;
}

//PHP EXERCISES
$sql="SELECT * FROM links WHERE description LIKE 'exercise 1%'";
$exercises1 = getData($sql);

//JavaScript EXERCISES
$sql="SELECT * FROM links WHERE description LIKE 'exercise 2%'";
$exercises2 = getData($sql);

//Database EXERCISES
$sql="SELECT * FROM links WHERE description LIKE 'exercise 3%'";
$exercises3 = getData($sql);

//Practice EXERCISES
$sql="SELECT * FROM links WHERE description LIKE 'exercise 5%'";
$exercises5 = getData($sql);

