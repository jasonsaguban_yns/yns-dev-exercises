-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2020 at 07:49 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exercise5-3`
--

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id` int(11) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `description` tinytext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id`, `link`, `description`) VALUES
(1, '../../HTML%20PHP/1-1.php', 'Exercise 1-1: Show Hello World.'),
(2, '../../HTML%20PHP/1-2.php', 'Exercise 1-2: The four basic operations of arithmetic.'),
(3, '../../HTML%20PHP/1-3.php', 'Exercise 1-3: Show the greatest common divisor.'),
(4, '../../HTML%20PHP/1-4.php', 'Exercise 1-4: Solve FizzBuzz problem.'),
(5, '../../HTML%20PHP/1-5.php', 'Exercise 1-5: Input date. Then show 3 days from inputted date and its day of the week.'),
(6, '../../HTML%20PHP/1-6/index.php', 'Exercise 1-6: Input user information. Then show it in next page.'),
(7, '../../HTML%20PHP/1-7/index.php', 'Exercise 1-7: Add validation in the user information form(required, numeric, character, mailaddress).'),
(8, '../../HTML%20PHP/1-8/index.php', 'Exercise 1-8: Store inputted user information into a CSV file.'),
(9, '../../HTML%20PHP/1-9/index.php', 'Exercise 1-9: Show the user information using table tags.'),
(10, '../../HTML%20PHP/1-10/index.php', 'Exercise 1-10: Upload images.'),
(11, '../../HTML%20PHP/1-11/index.php', 'Exercise 1-11: Show uploaded images in the table.'),
(12, '../../HTML%20PHP/1-12/index.php', 'Exercise 1-12: Add pagination in the list page.'),
(13, '../../HTML%20PHP/1-13/index.php', 'Exercise 1-13: Create login form and embed it into the system that you developed'),
(14, '../../JavaScript/2-2/index.html', 'Exercise 2-2: Confirm dialog and redirection'),
(15, '../../JavaScript/2-3/index.html', 'Exercise 2-3: The four basic operations of arithmetic'),
(16, '../../JavaScript/2-4/index.html', 'Exercise 2-4: Show prime numbers.'),
(17, '../../JavaScript/2-5/index.html', 'Exercise 2-5: Input characters in text box and show it in label.'),
(18, '../../JavaScript/2-6/index.html', 'Exercise 2-6: Press button and add a label below button.'),
(19, '../../JavaScript/2-7/index.html', 'Exercise 2-7: Show alert when you click an image.'),
(20, '../../JavaScript/2-8/index.html', 'Exercise 2-8: Show alert when you click link.'),
(21, '../../JavaScript/2-9/index.html', 'Exercise 2-9: Change text and background color when you press buttons.'),
(22, '../../JavaScript/2-10/index.html', 'Exercise 2-10: Scroll screen when you press buttons.'),
(23, '../../JavaScript/2-11/index.html', 'Exercise 2-11: Change background color using animation.'),
(24, '../../JavaScript/2-12/index.html', 'Exercise 2-12: Show another image when you mouse over an image. Then show the original image when you mouse out.'),
(25, '../../JavaScript/2-13/index.html', 'Exercise 2-13: Change size of images when you press buttons.'),
(26, '../../JavaScript/2-14/index.html', 'Exercise 2-14: Show images according to the options in combo box.'),
(27, '../../JavaScript/2-15/index.html', 'Exercise 2-15: Show current date and time in real time.'),
(29, '../../Database/3-4/index.php', 'Exercise 3-4: Solve problems using SQL'),
(30, '../../Database/3-5/index.php', 'Exercise 3-5: Use database in the applications that you developed.'),
(31, '../../Practice/5-1/login.php', 'Exercise 5-1: Quiz with three multiple choices'),
(32, '../../Practice/5-2/index.php', 'Exercise 5-2: Calendar');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
