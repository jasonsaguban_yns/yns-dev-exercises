<?php require_once 'db.inc.php' ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>5-3</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body class="h-100 bg-dark">
<div class="container-sm mt-5 bg-white">
    <div class="row">
        <div class="col-3">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link active my-3" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">HTML-PHP Exercises</a>
                <a class="nav-link my-3" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Javascript Exercises</a>
                <a class="nav-link my-3" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Database Exercises</a>
                <a class="nav-link my-3" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Practice Exercises</a>
            </div>
        </div>
        
        <div class="col-9">
            <div class="tab-content" id="v-pills-tabContent">

                <div class="tab-pane fade show active my-3" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                    <h3 class="mb-2">PHP Exercises</h3>
                    <div class="list-group ml-3">
                        <?php foreach($exercises1 as $exercise1): ?>
                        <a href=<?= $exercise1['link']?> target="_blank" class="list-group-item list-group-item-action link-success"><?= $exercise1['description']?></a>
                        <?php endforeach ?>
                    </div>
                </div>

                <div class="tab-pane fade my-3" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                <h3 class="mb-2">JavaScript Exercises</h3>
                    <div class="list-group ml-3">
                        <a href="../../JavaScript/2-1/index.html" target="_blank" class="list-group-item list-group-item-action link-success">Exercise 2-1: Show alert.</a>
                        <?php foreach($exercises2 as $exercise2): ?>
                        <a href=<?= $exercise2['link']?> target="_blank" class="list-group-item list-group-item-action link-success"><?= $exercise2['description']?></a>
                        <?php endforeach ?>
                    </div>
                </div>

                <div class="tab-pane fade my-3" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                <h3 class="mb-2">Database Exercises</h3>
                    <div class="list-group ml-3">
                        <?php foreach($exercises3 as $exercise3): ?>
                        <a href=<?= $exercise3['link']?> target="_blank" class="list-group-item list-group-item-action link-success"><?= $exercise3['description']?></a>
                        <?php endforeach ?>
                    </div>
                </div>

                <div class="tab-pane fade my-3" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                <h3 class="mb-2">Practice Exercises</h3>
                    <div class="list-group ml-3">
                        <?php foreach($exercises5 as $exercise5): ?>
                        <a href=<?= $exercise5['link']?> target="_blank" class="list-group-item list-group-item-action link-success"><?= $exercise5['description']?></a>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>
</body>
</html>