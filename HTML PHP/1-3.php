<!-- start time 2:25pm -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 1-3</title>
</head>
<body>
    <form action="" method="post">
        <h1>Greatest Common Divisor</h1>
        <label for="num1">First Number:</label>
        <input type="text" name="num1">
        <label for="num2">Second Number:</label>
        <input type="text" name="num2">
        <br><br>
        <button type='submit'>Submit</button>
        <br><br>
        <label for="answer">The Answer is: 
            <?php 
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $num1=$_POST['num1'];
                $num2=$_POST['num2'];

                while($num2 != 0){
                    $remainder = $num1 % $num2;
                    $num1 = $num2;
                    $num2 = $remainder;
                }
                echo $num1;
            }      
            ?> 
        </label>
    </form>
</body>
</html>
<!-- end time 3:08 -->



