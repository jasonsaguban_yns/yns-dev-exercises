<!-- start time 3:09pm -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 1-4</title>
</head>
<body>
    <form action="" method="post">
        <h1>Solve FizzBuzz problem.</h1>
        <label for="num1">Desired End Number:</label>
        <input type="text" name="num1">
        <br><br>
        <button type='submit'>Submit</button>
        <br><br>
        <label for="answer">The Result are: <br> 
            <?php 
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $num1=$_POST['num1'];
                $answer= [];

                for($i = 1; $i <= $num1; $i++) {

                    if($i % 3 == 0 && $i % 5 == 0){
                        $pushAnswer="FizzBuzz";
                    }elseif($i % 3 == 0 ){
                        $pushAnswer="Fizz";
                    }elseif($i % 5 == 0 ){
                        $pushAnswer="Buzz";
                    }else{
                        $pushAnswer = $i;
                    }

                    array_push($answer ,$pushAnswer);
                }

                foreach($answer as $ans){
                    echo $ans .'<br>';
                }

            }      
            ?> 
        </label>
    </form>
</body>
</html>

<!-- end time 3:19pm -->
