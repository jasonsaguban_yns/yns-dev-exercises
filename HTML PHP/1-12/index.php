<!-- start time 4:33pm -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 1-12</title>
</head>
<body>
<h1>Add pagination in the list page.</h1>
<br><br>
<?php

if(isset($_GET['error'])){
    if ($_GET['error'] == "empty") 
    {
        echo "<h3 style='color:red'>Fill in all fields! <h3>";		
    }
    elseif ($_GET['error'] == "invalid_email") 
    {
        echo "<h3 style='color:red'>Invalid Email! <h3>";	
    }
    elseif ($_GET['error'] == "invalid_fname"){
        echo "<h3 style='color:red'>Invalid First name! <h3>";
    }	
    elseif ($_GET['error'] == "invalid_lname") 
    {
        echo "<h3 style='color:red'>Invalid Last name! <h3>";			
    }
}
?>
<form action="1-12.inc.php" method="post" enctype="multipart/form-data">
    <label for="firstName">First Name:</label>
    <input type="text" name="firstName" placeholder="Enter your First Name" value= <?= isset($_GET['fname']) ? $_GET['fname'] : ''; ?> >
    <br>
    <label for="lastName">Last Name:</label>
    <input type="text" name="lastName" placeholder="Enter your Last Name" value= <?= isset($_GET['lname']) ? $_GET['lname'] : ''; ?>>
    <br>
    <label for="age">Age</label>
    <input type="number" name="age" placeholder="Enter your age" value= <?= isset($_GET['age']) ? $_GET['age'] : ''; ?>>
    <br>
    <label for="email">Email</label>
    <input type="email" name="email" placeholder="Enter your valid email" value= <?= isset($_GET['mail']) ? $_GET['mail'] : ''; ?>>
    <br>
    <label for="file">Profile Picture</label>
    <input type="file" name="file">
    <br>
    <button type="submit">Submit</button>
</form>
    
</body>
</html>