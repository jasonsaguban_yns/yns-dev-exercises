<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 1-12</title>
</head>
<body>
<h1 align ='center'>Add pagination in the list page.</h1>

<br><br>
<table border='1' cellpadding='10px' cellspacing='10px' width='100%'>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Age</th>
        <th>Email</th>
        <th>Profile Image</th>
    </tr>

    <?php
    $file=fopen('userinfo.csv','r');

    $fileRowCount=count(file('userinfo.csv',FILE_SKIP_EMPTY_LINES));
    $max_perPage = 10;
    $noOfPages =ceil($fileRowCount / $max_perPage);

    $pageNumber = $_GET['page'];

    if(!isset($pageNumber)){
        header('Location: 1-12.php?page=1');
    }
    
    if($pageNumber > '1'){
        $count = 0 ;
        $multiplier = $max_perPage * $pageNumber  - $max_perPage;
        
        while($count < $multiplier){ 
            $data =[fgetcsv($file)];
            $count++;
        }
    }

    $count2 = 0 ;
    while($count2 < $max_perPage): 
        $data =[fgetcsv($file)];

        if (feof($file)) {
            break;
        }

    ?>

    <tr>
        <td><?= $data[0][0] ?></td>
        <td><?= $data[0][1] ?></td>
        <td><?= $data[0][2] ?></td>
        <td><?= $data[0][3] ?></td>
        <td><img src="<?= $data[0][4] ?>" style="width:30% "></td>
    </tr>

    <?php  $count2++; endwhile; ?>

</table>

<?php

if($pageNumber != 1){
    $prev = "1-12.php?page=" . ($pageNumber - 1);
}else{
    $prev = '';
}

if($pageNumber < $noOfPages){
    $next = "1-12.php?page=" . ($pageNumber + 1);
} else{
    $next = '';
}

?>
<div style="float:right">
    <a style="padding-right:10px" href=<?= $prev ?>>Previous</a>

    <?php for($i=1; $i < $noOfPages + 1; $i++): ?>
    <a href=<?= "1-12.php?page=".$i ?> > <?= $i ?> </a>
    <?php endfor; ?>

    <a style="padding-left:10px" href=<?= $next ?>>Next</a>

    <br><br><br>
    <form action='index.php'>
        <input type='submit' value='Insert another data'/>
    </form>
</div>


    
</body>
</html>