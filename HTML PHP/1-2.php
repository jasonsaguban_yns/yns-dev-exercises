<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 1-2</title>
</head>
<body>
    <form action="1-2.php" method="post">
        <h1>The four basic operations of arithmetic.</h1>
        <label for="num1">First Number:</label>
        <input type="text" name="num1">
        <label for="num2">Second Number:</label>
        <input type="text" name="num2">
        <br><br>
        <button type="submit" name="add">Add</button>
        <button type="submit" name="sub">Subtract</button>
        <button type="submit" name="mul">Multiply</button>
        <button type="submit" name="div">Divide</button>
        <br><br>
        <label for="answer">The Answer is: 
            <?php 
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $num1=$_POST['num1'];
                $num2=$_POST['num2'];
        
                if(isset($_POST['add'])){
                    $answer= $num1 + $num2;
                    echo $answer;
                }
        
                if(isset($_POST['sub'])){
                    $answer= $num1 - $num2;
                    echo $answer;
                }
                if(isset($_POST['mul'])){
                    $answer= $num1 * $num2;
                    echo $answer;
                }
                if(isset($_POST['div'])){
                    $answer= $num1 / $num2;
                    echo $answer;
                }
            }      
            ?> 
        </label>
    </form>
</body>
</html>




