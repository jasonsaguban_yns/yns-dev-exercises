<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $username= trim($_POST['username']);
    $password= trim($_POST['password']);
    $firstName = trim($_POST['firstName']);
    $lastName = trim($_POST['lastName']);
    $age = trim($_POST['age']);
    $email = trim($_POST['email']);

    if($_FILES['file']['size'] != 'int(0)'){
        $file = $_FILES['file'];
        $fileName=$_FILES['file']['name'];
        $fileTmp=$_FILES['file']['tmp_name'];
        $fileSize=$_FILES['file']['size'];
        $fileError=$_FILES['file']['error'];
        $fileType=$_FILES['file']['type'];

        $fileExt = explode('.',$fileName);
        $fileActualExt = strtolower(end($fileExt));

        $allowed = ['jpg','jpeg','png'];

        if (in_array($fileActualExt,$allowed)) {
            if ($fileError === 0 ) {
                if ($fileSize < 3000000) {
                    $newFileName = uniqid('',true).".".$fileActualExt;
                    $fileDestination = 'images/'.$newFileName;
                    move_uploaded_file($fileTmp,$fileDestination);
                }else{
                    header("Location: index.php?error=bigfile&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
                    exit();
                }
            }else{
                header("Location: index.php?error=file_error&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
                exit();
            }
        }else{
            header("Location: index.php?error=filetype&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
            exit();
        }
    } else{
        $fileName = $fileDestination = NULL;
    }
    $usernames=[];
    $file=fopen('userinfo.csv','r');
    while(! feof($file)){
        $dataOfFile =[fgetcsv($file)];
        $usernameInFile = $dataOfFile[0][0];
        array_push($usernames,$usernameInFile);
    }
    fclose($file);

    if(empty($firstName) || empty($lastName) || empty($age) || empty($email) || empty($username) || empty($password)){
        header("Location: index.php?error=empty&uid=".$username."&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
		exit();
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        header("Location: index.php?error=invalid_email&uid=".$username."&fname=".$firstName."&lname=".$lastName."&age=".$age);
        exit();	
	} elseif (!preg_match("/^[a-zA-Z]*$/",$firstName)) {
        header("Location: index.php?error=invalid_fname&uid=".$username."&mail=".$email."&lname=".$lastName."&age=".$age);
        exit();
	} elseif (!preg_match("/^[a-zA-Z]*$/",$lastName)) {
        header("Location: index.php?error=invalid_lname&uid=".$username."&mail=".$email."&fname=".$firstName."&age=".$age);
        exit();
    } elseif (!preg_match("/^[a-zA-Z0-9]*$/",$username)) {
        header("Location: index.php?error=invalid_uid&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
		exit();
    } elseif (in_array($username,$usernames)) {
        header("Location: index.php?error=uid_taken&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
		exit();
    } elseif (strlen($password) < 8 ) {
        header("Location: index.php?error=invalid_pass&uid=".$username."&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
		exit();
    } else{
        $newPassword = password_hash($password,PASSWORD_DEFAULT);
        $data=[$username,$newPassword,$firstName,$lastName,$age,$email,$fileDestination];
        $file=fopen('userinfo.csv','a');
        fputcsv($file,$data);
        fclose($file);

        header("Location: login.php?register=success");
		exit();

    }
}else{
    header("Location: index.php");
    exit();
}