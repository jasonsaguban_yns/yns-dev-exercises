<!-- start time 4:33pm -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 1-13</title>
</head>
<body>
<h3>Create login form and embed it into the system that you developed</h3>
<br><br>
<div align='center'>
    <?php

    if(isset($_GET['error'])){
        if ($_GET['error'] == "empty"){
            echo "<h3 style='color:red'>Fill in all fields! <h3>";		
        } elseif ($_GET['error'] == "invalid_email") {
            echo "<h3 style='color:red'>Invalid Email! <h3>";	
        } elseif ($_GET['error'] == "invalid_fname"){
            echo "<h3 style='color:red'>Invalid First name! <h3>";
        } elseif ($_GET['error'] == "invalid_lname"){
            echo "<h3 style='color:red'>Invalid Last name! <h3>";			
        } elseif ($_GET['error'] == "invalid_uid"){
            echo "<h3 style='color:red'>Invalid Username! <h3>";
        } elseif ($_GET['error'] == "uid_taken"){
            echo "<h3 style='color:red'>Username is already taken! <h3>";
        } elseif ($_GET['error'] == "invalid_pass"){
            echo "<h3 style='color:red'>Password must be 8 or more characters! <h3>";			
        } elseif ($_GET['error'] == "filetype"){
            echo "<h3 style='color:red'>Invalid picture! (jpg,jpeg,png only) <h3>";
        } elseif ($_GET['error'] == "file_error"){
            echo "<h3 style='color:red'>File error, select another picture! <h3>";
        } elseif ($_GET['error'] == "bigfile"){
            echo "<h3 style='color:red'>Picture must be less than 3mb! <h3>";			
        }
    }
    ?>
    <form action="1-13.register.inc.php" method="post" enctype="multipart/form-data">
        <label for="username">Username:</label>
        <input type="text" name="username" placeholder="Enter a username" value= <?= isset($_GET['uid']) ? $_GET['uid'] : ''; ?> >
        <br><br>
        
        <label for="password">Password:</label>
        <input type="password" name="password" placeholder="Enter a password">
        <br><br><br>
        
        <label for="firstName">First Name:</label>
        <input type="text" name="firstName" placeholder="Enter your First Name" value= <?= isset($_GET['fname']) ? $_GET['fname'] : ''; ?> >
        <br><br>

        <label for="lastName">Last Name:</label>
        <input type="text" name="lastName" placeholder="Enter your Last Name" value= <?= isset($_GET['lname']) ? $_GET['lname'] : ''; ?>>
        <br><br>

        <label for="age">Age</label>
        <input type="number" name="age" placeholder="Enter your age" value= <?= isset($_GET['age']) ? $_GET['age'] : ''; ?>>
        <br><br>

        <label for="email">Email</label>
        <input type="email" name="email" placeholder="Enter your valid email" value= <?= isset($_GET['mail']) ? $_GET['mail'] : ''; ?>>
        <br><br>

        <label for="file">Profile Picture</label>
        <input type="file" name="file">
        <br><br><br>

        <button type="submit">Register</button>
    </form>
    <br>
    <a href="login.php">Already registered? Login now!</a>
</div>   
</body>
</html>