<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $username= trim($_POST['username']);
    $password= trim($_POST['password']);

    if(empty($username) || empty($password)){
        header("Location: login.php?error=empty&uid=".$username);
		exit();
    } elseif (!preg_match("/^[a-zA-Z0-9]*$/",$username)) {
        header("Location: login.php?error=invalid_uid");
		exit();
    } else{
        $file=fopen('userinfo.csv','r');
        while(! feof($file)){
            $dataOfFile =[fgetcsv($file)];
            $usernameInFile = $dataOfFile[0][0];
            $passwordInFile = $dataOfFile[0][1];
            $data[]=['username' => $usernameInFile , 'password' => $passwordInFile];
        }

        for ($i=0; $i < count($data) ; $i++) { 
            if(in_array($username,$data[$i])) {
                $hashedPassword= $data[$i]['password'];
                if(password_verify($password,$hashedPassword)){
                    header("Location: 1-13.php");
                    exit();
                } else{
                    header("Location: login.php?error=wrong_pass&uid=".$username);
                    exit();
                }
                break;
            } else{
                $userRegistered = "USER NOT FOUND";
            }
        }
        
        if($userRegistered === "USER NOT FOUND"){
            header("Location: login.php?error=user_notfound&uid=".$username);
            exit();
        }
    }
} else{
    header("Location: login.php");
    exit();
}

