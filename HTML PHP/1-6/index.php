<!-- start time 4:05pm -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 1-6</title>
</head>
<body>
<h1>Input user information. Then show it in next page.</h1>
<br><br>
<form action="1-6.inc.php" method="post">
    <label for="firstName">First Name:</label>
    <input type="text" name="firstName" placeholder="Enter your First Name">
    <br>
    <label for="lastName">Last Name:</label>
    <input type="text" name="lastName" placeholder="Enter your Last Name">
    <br>
    <label for="age">Age</label>
    <input type="number" name="age" placeholder="Enter your age">
    <br>
    <label for="email">Email</label>
    <input type="email" name="email" placeholder="Enter your valid email">
    <br>
    <button type="submit">Submit</button>
</form>
    
</body>
</html>

<!-- end time 4:30pm -->