<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 1-11</title>
</head>
<body>
<h1>Show uploaded images in the table.</h1>
<br><br>
<table border='1' cellpadding='10px' cellspacing='10px' width='100%'>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Age</th>
        <th>Email</th>
        <th>Profile Image</th>
    </tr>
    <?php
    $file=fopen('userinfo.csv','r');
    while(! feof($file)):
    $data =[fgetcsv($file)];
    ?>
    <tr>
    <td><?= $data[0][0] ?></td>
    <td><?= $data[0][1] ?></td>
    <td><?= $data[0][2] ?></td>
    <td><?= $data[0][3] ?></td>
    <td><img src="<?= $data[0][4] ?>" style="width:30% "></td>
    </tr>
    <?php endwhile; fclose($file); ?>
</table>

<br><br><br>
<form action='index.php'>
    <input type='submit' value='Insert another data'/>
</form>
    
</body>
</html>