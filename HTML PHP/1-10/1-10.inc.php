<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $firstName = trim($_POST['firstName']);
    $lastName = trim($_POST['lastName']);
    $age = trim($_POST['age']);
    $email = trim($_POST['email']);

    if($_FILES['file']['size'] != 'int(0)'){
        $file = $_FILES['file'];
        $fileName=$_FILES['file']['name'];
        $fileTmp=$_FILES['file']['tmp_name'];
        $fileSize=$_FILES['file']['size'];
        $fileError=$_FILES['file']['error'];
        $fileType=$_FILES['file']['type'];

        $fileExt = explode('.',$fileName);
        $fileActualExt = strtolower(end($fileExt));

        $allowed = ['jpg','jpeg','png'];

        if (in_array($fileActualExt,$allowed)) {
            if ($fileError === 0 ) {
                if ($fileSize < 3000000) {
                    $newFileName = uniqid('',true).".".$fileActualExt;
                    $fileDestination = 'images/'.$newFileName;
                    move_uploaded_file($fileTmp,$fileDestination);
                }else{
                    header("Location: index.php?error=bigfile&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
                    exit();
                }
            }else{
                header("Location: index.php?error=fileerror&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
                exit();
            }
        }else{
            header("Location: index.php?error=filetpye&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
            exit();
        }
    } else{
        $fileName = $fileDestination = NULL;
    }

    if(empty($firstName) || empty($lastName) || empty($age) || empty($email)){
        header("Location: index.php?error=empty&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
		exit();
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        header("Location: index.php?error=invalid_email&fname=".$firstName."&lname=".$lastName."&age=".$age);
        exit();	
	  } elseif (!preg_match("/^[a-zA-Z]*$/",$firstName)) {
        header("Location: index.php?error=invalid_fname&mail=".$email."&lname=".$lastName."&age=".$age);
        exit();
	  } elseif (!preg_match("/^[a-zA-Z]*$/",$lastName)) {
        header("Location: index.php?error=invalid_lname&mail=".$email."&fname=".$firstName."&age=".$age);
        exit();
    } else{

        $data=[$firstName,$lastName,$age,$email,$fileDestination];
        $file=fopen('userinfo.csv','a');
        fputcsv($file,$data);
        fclose($file);

        echo "User has been succesfuly save into csv file!<br>";

        echo "First Name: ".$firstName ."<br>";
        echo "Last Name: ".$lastName ."<br>";
        echo "Age: ".$age ."<br>";
        echo "Email: ".$email ."<br>";
        echo "Profile Picture: ".$fileName."<br>";
        echo "<br><form action='index.php'>
        <input type='submit' value='Add another data'/>
        </form>";
        echo "<form action='1-10.php'>
              <input type='submit' value='Check all user information'/>
              </form>";
    }
}