<?php
require_once 'db.inc.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $username= trim($_POST['username']);
    $password= trim($_POST['password']);
    $firstName = trim($_POST['firstName']);
    $lastName = trim($_POST['lastName']);
    $age = trim($_POST['age']);
    $email = trim($_POST['email']);

    if($_FILES['file']['size'] != 'int(0)'){
        $file = $_FILES['file'];
        $fileName=$_FILES['file']['name'];
        $fileTmp=$_FILES['file']['tmp_name'];
        $fileSize=$_FILES['file']['size'];
        $fileError=$_FILES['file']['error'];
        $fileType=$_FILES['file']['type'];

        $fileExt = explode('.',$fileName);
        $fileActualExt = strtolower(end($fileExt));

        $allowed = ['jpg','jpeg','png'];

        if (in_array($fileActualExt,$allowed)) {
            if ($fileError === 0 ) {
                if ($fileSize < 3000000) {
                    $savePicture="SAVE PICTURE";
                }else{
                    header("Location: index.php?error=bigfile&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
                    exit();
                }
            }else{
                header("Location: index.php?error=file_error&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
                exit();
            }
        }else{
            header("Location: index.php?error=filetype&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
            exit();
        }
    } else{
        $fileName = $fileDestination = NULL;
    }

    //Checking if the username is already taken
    $sql='SELECT * FROM users WHERE username= :username';
    $stmt= $pdo->prepare($sql);
    $stmt->execute(['username' => $username]);
    $userCount = $stmt->rowCount();

    //Validations
    if(empty($firstName) || empty($lastName) || empty($age) || empty($email) || empty($username) || empty($password)){
        header("Location: index.php?error=empty&uid=".$username."&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
		exit();
    } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        header("Location: index.php?error=invalid_email&uid=".$username."&fname=".$firstName."&lname=".$lastName."&age=".$age);
        exit();	
	} elseif (!preg_match("/^[a-zA-Z]*$/",$firstName)) {
        header("Location: index.php?error=invalid_fname&uid=".$username."&mail=".$email."&lname=".$lastName."&age=".$age);
        exit();
	} elseif (!preg_match("/^[a-zA-Z]*$/",$lastName)) {
        header("Location: index.php?error=invalid_lname&uid=".$username."&mail=".$email."&fname=".$firstName."&age=".$age);
        exit();
    } elseif (!preg_match("/^[a-zA-Z0-9]*$/",$username)) {
        header("Location: index.php?error=invalid_uid&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
		exit();
    } elseif ($userCount > 0) {
        header("Location: index.php?error=uid_taken&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
		exit();
    } elseif (strlen($password) < 8 ) {
        header("Location: index.php?error=invalid_pass&uid=".$username."&mail=".$email."&fname=".$firstName."&lname=".$lastName."&age=".$age);
		exit();
    } else{
    //If no errors ->proceed();
        
        //uploading picture
        if(!empty($savePicture)){
            $newFileName = uniqid('',true).".".$fileActualExt;
            $fileDestination = 'images/'.$newFileName;
            move_uploaded_file($fileTmp,$fileDestination);
        }
        
        //hashing the password
        $newPassword = password_hash($password,PASSWORD_DEFAULT);
        //inserting the data into the database
        $sql='INSERT INTO users(first_name,last_name,age,username,password,email,profile_picture) 
        VALUES(:first_name,:last_name,:age,:username,:password,:email,:profile_picture)';
        $stmt= $pdo->prepare($sql);
        $stmt->execute([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'age' => $age,
            'username' => $username,
            'password' => $newPassword,
            'email' => $email,
            'profile_picture' => $fileDestination
            ]);
        header("Location: login.php?register=success");
		exit();

    }
}else{
    header("Location: index.php");
    exit();
}