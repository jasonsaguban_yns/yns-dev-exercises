<?php
require_once 'db.inc.php';
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    $username= trim($_POST['username']);
    $password= trim($_POST['password']);

    if(empty($username) || empty($password)){
        header("Location: login.php?error=empty&uid=".$username);
		exit();
    } elseif (!preg_match("/^[a-zA-Z0-9]*$/",$username)) {
        header("Location: login.php?error=invalid_uid");
		exit();
    } else{
        $sql='SELECT * FROM users WHERE username= :username';
        $stmt= $pdo->prepare($sql);
        $stmt->execute(['username' => $username]);
        $userCount = $stmt->rowCount();
        $userData= $stmt->fetch();

        if($userCount < 1){
            header("Location: login.php?error=user_notfound&uid=".$username);
            exit();
        }else{
            if(password_verify($password,$userData['password'])){
                header("Location: 3-5.php");
                exit();
            }else{
                header("Location: login.php?error=wrong_pass&uid=".$username);
                exit();
            }

        }
    }
} else{
    header("Location: login.php");
    exit();
}

