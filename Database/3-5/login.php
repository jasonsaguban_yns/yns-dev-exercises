<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 3-5</title>
</head>
<body>

<div align='center'>
    <h3>Use database in the applications that you developed.</h3>
    <br><br>
    <h1>L O G I N</h1>
    <?php
        if(isset($_GET['error'])){
            if ($_GET['error'] == "empty"){
                echo "<h3 style='color:red'>Fill in all fields! <h3>";		
            } elseif ($_GET['error'] == "invalid_uid") {
                echo "<h3 style='color:red'>Invalid Username! <h3>";	
            } elseif ($_GET['error'] == "wrong_pass") {
                echo "<h3 style='color:red'>Wrong Password! <h3>";	
            } elseif ($_GET['error'] == "user_notfound") {
                echo "<h3 style='color:red'>User not found! <h3>";	
            }  
        }
        if(isset($_GET['register'])){
            echo "<h3 style='color:green'>You are now registered and, be able to login! <h3>";
        }
    ?>
    <form action="login.inc.php" method="post">
        <label for="username">Username: </label>
        <input type="text" name="username" value= <?= isset($_GET['uid']) ? $_GET['uid'] : ''; ?>>
        <br>
        <br>
        <label for="password">Password: </label>
        <input type="password" name="password">
        <br>
        <br>
        <button type="submit">Login</button>
    </form>
    <br>
    <a href="index.php">Not yet registered? Register now!</a>
</div>
</body>
</html>