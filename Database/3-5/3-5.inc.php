<?php
require_once 'db.inc.php';

//getting the number of records
$sql='SELECT * FROM users LIMIT 0,10';
$stmt= $pdo->prepare($sql);
$stmt->execute();
$userCount = $stmt->rowCount();

$maxPerPage = 10;
$noOfPages = ceil($userCount / $maxPerPage);

if(!isset($_GET['page'])){
    $page= 1;
    header('Location: 3-5.php?page='.$page);

} else{
    $page = $_GET['page'];
}

$startingLimitNumber = ($page -1 ) * $maxPerPage;

$sql="SELECT * FROM users LIMIT " . $startingLimitNumber . ",". $maxPerPage ;
$stmt= $pdo->prepare($sql);
$stmt->execute();
$data = $stmt->fetchAll(); 

//organizing previous and next button pagination
if($page != 1){
    $prev = "3-5.php?page=" . ($page - 1);
}else{
    $prev = '';
}

if($page < $noOfPages){
    $next = "3-5.php?page=" . ($page + 1);
} else{
    $next = '';
}