<?php require_once '3-5.inc.php '?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise 3-5</title>
</head>
<body>
<h3 align='center'>Use database in the applications that you developed.</h3>
<br><br>
<form style="float:right" action='login.php'>
        <input type='submit' value='Logout'/>
</form>
<table border='1' cellpadding='10px' cellspacing='10px' width='100%'>
    <tr>
        <th>Username</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Age</th>
        <th>Email</th>
        <th>Profile Image</th>
        <th>Date Created</th>
    </tr>

    <?php
        $count2 = 0 ;
        while($count2 < count($data)): 
    ?>
    <tr>
        <td><?= $data[$count2]['username'] ?></td>
        <td><?= $data[$count2]['first_name'] ?></td>
        <td><?= $data[$count2]['last_name'] ?></td>
        <td><?= $data[$count2]['age'] ?></td>
        <td><?= $data[$count2]['email'] ?></td>
        <td><img src="<?= $data[$count2]['profile_picture'] ?>" style="width:30% "></td>
        <td><?= $data[$count2]['date_created'] ?></td>
    </tr>
    <?php  $count2++; endwhile; ?>

</table>
<div style="float:right">
<a style="padding-right:10px" href=<?= $prev ?>>Previous</a>

<?php for($page=1; $page <= $noOfPages; $page++): ?>
<a href="3-5.php?page=<?=$page?>" style="padding-left:10px"> <?=$page?> </a>
<?php endfor ?>

<a style="padding-left:10px" href=<?= $next ?>>Next</a>
</div>
 
</body>
</html>