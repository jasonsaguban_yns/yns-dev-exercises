<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>3-4</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/css/bootstrap.min.css" integrity="sha384-r4NyP46KrjDleawBgD5tp8Y7UzmLA05oM1iAEQ17CSuDqnUK2+k9luXQOfXJCJ4I" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>

<div class="container mt-5">
    <div class="card">
        <h6>1) Retrieve employees whose last name start with "K".</h6>
        <p>SELECT * FROM employees WHERE last_name REGEXP '^k';</p>

        <h6>2) Retrieve employees whose last name end with "i".</h6>
        <p>SELECT * FROM employees WHERE last_name REGEXP 'i$';</p>

        <h6>3) Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date.</h6>
        <p> SELECT CONCAT(first_name, ' ', middle_name, ' ', last_name) AS 'full_name',hire_dateFROM employees WHERE (hire_date BETWEEN '2015-1-1' AND '2015-3-31') ORDER BY hire_date ASC</p>

        <h6>4) Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show.</h6>
        <p>SELECT e.last_name AS 'Employee',b.last_name AS 'Boss' FROM employees eJOIN employees b ON b.id = e.boss_id</p>

        <h6>5) Retrieve employee's last name who belong to Sales department ordered by descending by last name.</h6>
        <p>SELECT e.last_name FROM employees e JOIN departments d ON d.id = e.department_id WHERE d.name = "Sales" ORDER BY e.last_name DESC</p>

        <h6>6) Retrieve number of employee who has middle name.</h6>
        <p>SELECT COUNT(middle_name) AS 'count_has_middle' FROM employees WHERE middle_name IS NOT NULL</p>

        <h6>7) Retrieve department name and number of employee in each department. You don't need to retrieve the department name which doesn't have employee.</h6>
        <p>SELECT d.name, COUNT(e.id) as 'Count(e.id)' FROM employees e JOIN departments d ON d.id = e.department_id GROUP BY d.name;</p>

        <h6>8) Retrieve employee's full name and hire date who was hired the most recently.</h6>
        <p>SELECT first_name, middle_name, last_name, hire_date FROM employees e WHERE hire_date IN (SELECT MIN(hire_date) FROM employees)</p>

        <h6>9) Retrieve department name which has no employee.</h6>
        <p>SELECT d.name, e.id FROM departments d LEFT JOIN employees e ON d.id = e.department_id WHERE e.department_id IS NULL   </p>

        <h6>10) Retrieve employee's full name who has more than 2 positions.</h6>
        <p>SELECT first_name, middle_name, last_name FROM employee_positions ep JOIN employees e ON ep.employee_id = e.id HAVING COUNT(ep.position_id) > 2</p>
    </div>
    
</div>    
    <script>
        $("h6").attr("class","card-title my-2 ml-2");
        $("p").attr("class","card-body ml-4 mb-4");
    </script>
</body>
</html>