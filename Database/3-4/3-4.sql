-- 1) Retrieve employees whose last name start with "K". 

    SELECT * FROM employees 
    WHERE last_name REGEXP '^k';

-- 2) Retrieve employees whose last name end with "i".

    SELECT * FROM employees 
    WHERE last_name REGEXP 'i$';

-- 3) Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date.

    SELECT 
    CONCAT(first_name, ' ', middle_name, ' ', last_name) AS 'full_name',
    hire_date
    FROM employees 
    WHERE (hire_date BETWEEN '2015-1-1' AND '2015-3-31') 
    ORDER BY hire_date ASC

-- 4) Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show.    

    SELECT 
    e.last_name AS 'Employee',
    b.last_name AS 'Boss' 
    FROM employees e
    JOIN employees b 
        ON b.id = e.boss_id

-- 5) Retrieve employee's last name who belong to Sales department ordered by descending by last name.

    SELECT
    e.last_name
    FROM employees e
    JOIN departments d 
        ON d.id = e.department_id
    WHERE d.name = "Sales"
    ORDER BY e.last_name DESC

-- 6) Retrieve number of employee who has middle name.

    SELECT 
    COUNT(middle_name) AS 'count_has_middle' 
    FROM employees 
    WHERE middle_name IS NOT NULL

-- 7) Retrieve department name and number of employee in each department. You don't need to retrieve the department name which doesn't have employee.

    SELECT
    d.name,
    COUNT(e.id) as 'Count(e.id)'
    FROM employees e
    JOIN departments d 
        ON d.id = e.department_id
    GROUP BY d.name;

-- 8) Retrieve employee's full name and hire date who was hired the most recently. 

    SELECT
    first_name,
    middle_name,
    last_name,
    hire_date
    FROM employees e
    WHERE hire_date IN (SELECT MIN(hire_date) FROM employees)

-- 9) Retrieve department name which has no employee.

    SELECT
    d.name,
    e.id
    FROM departments d 
    LEFT JOIN employees e
        ON d.id = e.department_id
    WHERE e.department_id IS NULL   
    
-- 10) Retrieve employee's full name who has more than 2 positions.

    SELECT
    first_name,
    middle_name,
    last_name
    FROM employee_positions ep
    JOIN employees e
        ON ep.employee_id = e.id
    HAVING COUNT(ep.position_id) > 2



